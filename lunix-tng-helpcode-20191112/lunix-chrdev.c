/*
 * lunix-chrdev.c
 *
 * Implementation of character devices
 * for Lunix:TNG
 *
 * < Your name here >
 *
 */

#include <linux/mm.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/list.h>
#include <linux/cdev.h>
#include <linux/poll.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <linux/ioctl.h>
#include <linux/types.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/mmzone.h>
#include <linux/vmalloc.h>
#include <linux/spinlock.h>

#include "lunix.h"
#include "lunix-chrdev.h"
#include "lunix-lookup.h"

/*
 * Global data
 */
struct cdev lunix_chrdev_cdev;

/*
 * Just a quick [unlocked] check to see if the cached
 * chrdev state needs to be updated from sensor measurements.
 */
static int lunix_chrdev_state_needs_refresh(struct lunix_chrdev_state_struct *state)
{
	struct lunix_sensor_struct *sensor;
	
	WARN_ON ( !(sensor = state->sensor));

	int ret;
	ret = 0;
	if((state->buf_timestamp < sensor->msr_data[state->type]->last_update))
	{
		ret = 1;
	}
	return ret;
}

/*
 * Updates the cached state of a character device
 * based on sensor data. Must be called with the
 * character device state lock held.
 */
static int lunix_chrdev_state_update(struct lunix_chrdev_state_struct *state)
{
	struct lunix_sensor_struct *sensor;
  	WARN_ON ( !(sensor = state->sensor));
	/*
	 * Grab the raw data quickly, hold the
	 * spinlock for as little as possible.
	 */
	
	/* Why use spinlocks? See LDD3, p. 119 */

	/*
	 * Any new data available?
	 */

	
	uint32_t tempData;
	tempData = 0;
	if(lunix_chrdev_state_needs_refresh(state))
	{
		spin_lock(&(sensor->lock));
		memcpy(&tempData, &(sensor->msr_data[state->type]->values[0]), sizeof(sensor->msr_data[state->type]->values[0]));
		state->buf_timestamp = sensor->msr_data[state->type]->last_update;
		spin_unlock(&(sensor->lock));
		//debug("Completed: tempData is %d", tempData, state->buf_timestamp);

	}
	else
	{
		return -EAGAIN;
	}

	/*
	 * Now we can take our time to format them,
	 * holding only the private state semaphore
	 */
		
	long result;
	result = 0;
	
	switch(state->type)
	{
		case BATT:
			if(tempData < 65536)
		       	result = lookup_voltage[tempData];	
			break;
		case TEMP:
		       	if(tempData < 65536)	
			result = lookup_temperature[tempData];
			break;
		case LIGHT:
			if(tempData < 65536)
			result = lookup_light[tempData];
			break;
		default:
			break;
	}
	
	int decimal;
	decimal = result % 1000;
	int integer;
	integer = result / 1000;
	sprintf(&(state->buf_data[0]), "%d.%d\n", integer, decimal);
	debug("Got value: %s\n", state->buf_data);

	return 0;
}

/*************************************
 * Implementation of file operations
 * for the Lunix character device
 *************************************/

static int lunix_chrdev_open(struct inode *inode, struct file *filp)
{
	/* Declarations */
	int major;
	int minor;
	int ret;
	int sensorNum;
	int sensorType;
	ret = -ENODEV;
	if ((ret = nonseekable_open(inode, filp)) < 0)
		goto out;

	/*
	 * Associate this open file with the relevant sensor based on
	 * the minor number of the device node [/dev/sensor<NO>-<TYPE>]
	 */
	major = imajor(inode);
	minor = iminor(inode);
	debug("Opening minor %d\n", minor);
	
	/* Allocate a new Lunix character device private state structure */
	sensorNum = minor / 8;
	sensorType = minor % 8;

	struct lunix_chrdev_state_struct *state;
	state = kmalloc(sizeof(struct lunix_chrdev_state_struct), GFP_KERNEL);
	memset(state, 0, sizeof(state));
	state->type = sensorType;
	state->sensor = &(lunix_sensors[sensorNum]);
	sema_init(&state->lock, 1);

	filp->private_data = state;
		
out:
	return ret;
}

static int lunix_chrdev_release(struct inode *inode, struct file *filp)
{
	/* not sure if we need these */
	int major;
	int minor;
	major = imajor(inode);
	minor = iminor(inode);
	/* */
	kfree(filp->private_data);
	debug("Released %d\n", minor);
	return 0;
}

static long lunix_chrdev_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	/* Why? */
	return -EINVAL;
}

static ssize_t lunix_chrdev_read(struct file *filp, char __user *usrbuf, size_t cnt, loff_t *f_pos)
{
	ssize_t ret;

	struct lunix_sensor_struct *sensor;
	struct lunix_chrdev_state_struct *state;
	int upd_ret;
	state = filp->private_data;
	WARN_ON(!state);

	sensor = state->sensor;
	WARN_ON(!sensor);
	ret = 0;
	/* Lock? */
	/*
	 * If the cached character device state needs to be
	 * updated by actual sensor data (i.e. we need to report
	 * on a "fresh" measurement, do so
	 */
	if(down_interruptible(&state->lock))
		return -ERESTARTSYS;
	if (*f_pos == 0) {
		while (lunix_chrdev_state_update(state) == -EAGAIN) {
			up(&state->lock);
			if(wait_event_interruptible(sensor->wq, (lunix_chrdev_state_needs_refresh(state))))
				return -ERESTARTSYS;
			if(down_interruptible(&state->lock))
				return -ERESTARTSYS;
			/* The process needs to sleep */
			/* See LDD3, page 153 for a hint */
		}
	}
	/* End of file */	
	/* Determine the number of cached bytes to copy to userspace */
	/* Auto-rewind on EOF mode? */
	
	int i;
	i = 0;
	int flag;
	flag = 1;
	while(flag){
		if(i==19){
			state->buf_data[19] = '\0';
			break;
		}
		if(state->buf_data[i] != '\0') i++;
		else{
			i++;
			break;
		}
	}
	
	int bytesToCopy = cnt;
	if((*f_pos + cnt) > i){
		bytesToCopy = i - *f_pos;
	}


	if(copy_to_user(&usrbuf[0], &(state->buf_data[*f_pos]), bytesToCopy))
		return -EFAULT;
	

	*f_pos += bytesToCopy;
	if(*f_pos == i) *f_pos = 0;
	ret = bytesToCopy;
	up(&state->lock);
out:
	return ret;
}

static int lunix_chrdev_mmap(struct file *filp, struct vm_area_struct *vma)
{
	return -EINVAL;
}

static struct file_operations lunix_chrdev_fops = 
{
        .owner          = THIS_MODULE,
	.open           = lunix_chrdev_open,
	.release        = lunix_chrdev_release,
	.read           = lunix_chrdev_read,
	.unlocked_ioctl = lunix_chrdev_ioctl,
	.mmap           = lunix_chrdev_mmap
};

int lunix_chrdev_init(void)
{
	/*
	 * Register the character device with the kernel, asking for
	 * a range of minor numbers (number of sensors * 8 measurements / sensor)
	 * beginning with LINUX_CHRDEV_MAJOR:0
	 */
	int ret;
	dev_t dev_no;
	unsigned int lunix_minor_cnt = lunix_sensor_cnt << 3;
	debug("initializing character device\n");
	cdev_init(&lunix_chrdev_cdev, &lunix_chrdev_fops);
	lunix_chrdev_cdev.owner = THIS_MODULE;
	
	dev_no = MKDEV(LUNIX_CHRDEV_MAJOR, 0);

	ret = register_chrdev_region(dev_no, lunix_minor_cnt, "lunix_driver");

	if (ret < 0) {
		debug("failed to register region, ret = %d\n", ret);
		goto out;
	}

	ret = cdev_add(&lunix_chrdev_cdev, dev_no, lunix_minor_cnt);	

	/* cdev_add? */
	if (ret < 0) {
		debug("failed to add character device\n");
		goto out_with_chrdev_region;
	}
	debug("completed successfully\n");
	return 0;

out_with_chrdev_region:
	unregister_chrdev_region(dev_no, lunix_minor_cnt);
out:
	return ret;
}

void lunix_chrdev_destroy(void)
{
	dev_t dev_no;
	unsigned int lunix_minor_cnt = lunix_sensor_cnt << 3;
		
	debug("entering\n");
	dev_no = MKDEV(LUNIX_CHRDEV_MAJOR, 0);
	cdev_del(&lunix_chrdev_cdev);
	unregister_chrdev_region(dev_no, lunix_minor_cnt);
	debug("leaving\n");
}
